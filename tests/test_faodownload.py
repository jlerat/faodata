#!/usr/bin/env python

import os
import unittest
import numpy as np
import datetime
import pandas as pd

from faodata import faodownload

class faodownloadTestCase(unittest.TestCase):

    def setUp(self):
        pass

    def test_get_databases(self):
        db = faodownload.get_databases()

        self.assertTrue('FAOSTAT' in list(db['label']))


    def test_get_datasets(self):
        db = faodownload.get_databases()

        for m in db['mnemonic']:
            
            print('\n.. retrieving datasets from db [%s]' % m)
            
            ds = faodownload.get_datasets(database=m)

            if not ds is None:
                print('  -> has %d datasets' % ds.shape[0])
            else:
                print('  -> no datasets')

        ds = faodownload.get_datasets(database='faostat')

        self.assertTrue('crop-prod' in list(ds['mnemonic']))


    def test_get_fields(self):
        database = 'faostat'
        dataset = 'crop-prod'

        fields = faodownload.get_fields(database, dataset)
        self.assertTrue(list(fields['mnemonic']) == [
                'm5312', 'm5419', 'm5510', 'm5525'])

    def test_get_data1(self):
        database = 'faostat'
        dataset = 'live-prod'
        field = 'm5111'
        country = 'USA'
        year = 2010

        d1 = faodownload.get_data(database, dataset, 
            field, country=country)
        self.assertTrue(d1.shape[0] >= 371)

        d2 = faodownload.get_data(database, dataset, field, year=year)
        self.assertTrue(d2.shape[0] >= 1200)

        d3 = faodownload.get_data(database, dataset, field, 
                    country=country, year=year)
        self.assertTrue(d3.shape[0] == 7)


    def test_get_data2(self):
        db = faodownload.get_databases()

        for m in db['mnemonic']:
            
            print('\n\nRetrieving datasets from database %s' % m)
            
            ds = faodownload.get_datasets(database=m)

            if not ds is None:

                for s in ds['mnemonic']:
                    print('\n  + Retrieving fields from dataset %s' % s)
                    df = faodownload.get_fields(m, s)

                    if not df is None:

                        for f in df['mnemonic']:
                            print('    + Retrieving data from field %s' % f)

                            d = faodownload.get_data(m, s, f, 
                                    year=2010, country='USA')
                            
                            if not d is None:
                                print('      has %d data points' % d.shape[0])
                            else:
                                print('      has no data points')

                    else:
                        print('    no fields')

            else:
                print('  no datasets')


if __name__ == "__main__":
    unittest.main()
