#!/usr/bin/env python

import os, re
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import basemap

from faodata import faodownload, faomap

# Create output directory
source_file = os.path.abspath(__file__)
FIMG = os.path.dirname(source_file)

# Get data
database = 'faostat'
dataset = 'live-prod'
field = 'm5111'
year = 2013
df = faodownload.get_data(database, dataset, field, year=year)

item = 'Cattle'
idx = df['Item'] == item
df = df.loc[idx, ['country', 'value']]

# Instantiate matplotlib objects
plt.close('all')
fig, ax = plt.subplots()

# Instantiate basemap objects
map = basemap.Basemap(ax=ax)
map.drawcoastlines(color='grey')
map.drawcountries(color='grey')

# Define categories
cat = [np.percentile(df['value'], pp) 
        for pp in range(10, 100, 10)]

# Plot data
faomap.plot(map, df, cat, ndigits=0)

# Plot decorations
ax.axis('off')
map.ax.legend(loc=3)
ax.set_title('%s population, %d' % (item, year),
        fontsize=15)
faomap.mapfooter(fig, database, dataset, field)

# Save plot
fig.set_size_inches((16, 8))
fig.tight_layout()
fp = re.sub('\\.py', '.png', source_file)
fig.savefig(fp)


