#!/usr/bin/env python

import os, re, sys
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from mpl_toolkits import basemap

from faodata import faodownload, faomap

# Create output directory
source_file = os.path.abspath(__file__)
FIMG = os.path.dirname(source_file)


# Instantiate matplotlib objects
plt.close('all')
fig, axs = plt.subplots(nrows=3, ncols=3)
axs = axs.flat[:]

# Define categories (Kg/Year/Inhabitant)
cat = [0, 10, 100, 500, 1000, 5000, 10000, 50000]

# Loop through years and draw plot
count = 0

for year in range(1988, 2013, 3):

    print('.. dealing with %d ..' % year)

    # Get population data
    database = 'faostat'
    dataset = 'pop'
    field = 'm3010'
    df_pop = faodownload.get_data(database, dataset, field, year=year)
    
    item = 'Total Population - Both sexes'
    idx = df_pop['Item'] == item
    df_pop = df_pop.loc[idx, ['country', 'value']]
 
    # Get Crop production data
    database = 'faostat'
    dataset = 'crop-prod'
    field = 'm5510'
    df_prod = faodownload.get_data(database, dataset, field, year=year)

    item = ['Wheat', 'Rice, paddy', 'Sugar cane']
    idx = df_prod['Item'].isin(item)
    df_prod = df_prod[idx].groupby('country').sum()
    df_prod = df_prod.reset_index()

    # Compute production by inhabitants
    df_prod = pd.merge(df_prod, df_pop, \
            suffixes = ['_prod', '_pop'], \
            how='inner', on='country')

    df_prod['value'] = df_prod['value_prod']/df_prod['value_pop']
    df_prod['value'] *= 1000./365.25

    # Instantiate basemap objects
    ax = axs[count]
    count += 1
    map = basemap.Basemap(ax=ax)
    map.drawcoastlines(color='grey')
    map.drawcountries(color='grey')

    # Plot data
    faomap.plot(map, df_prod, cat, ndigits=0)
    
    # Plot decorations
    ax.axis('off')
    #map.ax.legend(loc=3)
    ax.set_title('%d' % year)

# Save plot
fig.set_size_inches((16, 8))
fig.tight_layout()
fp = re.sub('\\.py', '.png', source_file)
fig.savefig(fp)


